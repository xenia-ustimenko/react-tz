import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {Actions, IDispatchProps} from '../Actions/Actions';
import {IStoreState} from '../Store/Store';

interface IStateProps {
  loginStatus: boolean;
  loading: boolean;
  login: string;
}
interface IState {
  inputLogin: string
}
type TProps = IDispatchProps & IStateProps;
type TState = IState;

class App extends React.Component<TProps, TState> {

  constructor(props: TProps) {
    super(props);
    this.state = {
      inputLogin: props.login
    };
  }
  componentWillReceiveProps(nextProps:TProps) {
    if (nextProps.login !== this.state.inputLogin) {
      this.setState({inputLogin: nextProps.login});
    }
  }
  handleClick = () => {
    const {actions} = this.props;
    actions.onClick(0);
  };

  handleLogin = () => {
    const {actions} = this.props;
    const {inputLogin} = this.state
    if (inputLogin !== '') {
      actions.onLogin(inputLogin);
    }
  };

  handleLogout = () => {
    const {actions} = this.props;
    actions.onLogout();
  };
  
  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ inputLogin: e.target.value });
  };

  render () {
    const {loginStatus, loading, login} = this.props;
    const {inputLogin} = this.state;
    return (
      <div>
        <h3>
          Boilerplate
        </h3>
        {
          // loading ?
          //   <p>Авторизация...</p> :
            loginStatus ?
              <p>
                Login success {login}
              </p> :
              <p>
                Logged out
              </p>
        }
        <input type="text" placeholder='Login' onChange={this.handleChange}
        value={inputLogin}/>
        <input className="btn btn-outline-secondary" disabled={loading} type="button" value="+" onClick={this.handleClick}/>
        <input className="btn btn-outline-primary" disabled={loading} type="button" value="login" onClick={this.handleLogin}/>
        <input className="btn btn-outline-warning" disabled={loading} type="button" value="logout" onClick={this.handleLogout}/>
      </div>
    );
  }
};

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    loginStatus: state.loginStatus,
    loading: state.loading,
    login: state.login
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
  return {
    actions: new Actions(dispatch)
  };
}

const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export {connectApp as App};
