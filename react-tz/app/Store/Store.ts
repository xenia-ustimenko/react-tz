import {Action, applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';

export interface IActionType extends Action {
  type: string;
  payload: any;
}

export interface IStoreState {
  data?: number;
  loginStatus: boolean;
  loading: boolean;
  login?: string;
}

const initialState = {
  get state(): IStoreState {
    return {
      loginStatus: false,
      loading: false,
      login: ""
    }
  }
}

function reducer (state: IStoreState = initialState.state, action: IActionType) {
  console.log(state);

  switch (action.type) {
    case ActionTypes.CLICK:
    return {
      ...state,
      data: (state.data || 0) + 1,
    };

    case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
    return {
      ...state,
      loading: true,
    };

    case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
    return {
      ...state,
      loginStatus: action.payload.data.authorized,
      loading: false,
    };

    case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
    return {
      ...state,
      loading: false,
      loginStatus: false,
    };

    case ActionTypes.LOGOUT:
    return {
      ...state,
      login: "",
      loginStatus: false,
    };
  }
  return state;
}

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

export {store as appStore};
